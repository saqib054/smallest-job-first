**Smallest Job First (SJF) Algorithm implementation in javascript**

Function takes jobs array with transaction time and index.
Returns the clock cycles of the process/job at specific index.
Implementation was part of one of my codility challenge.
Function Call with sample input, sjf([10, 3, 15, 8, 7], 3)